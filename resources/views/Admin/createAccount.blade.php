@extends('layouts.app')

@section('content')
<div class="container">
    <form action="/createAccount" method="post">
        @csrf
        <div class="card">
        <div class="card-body">
         <h3>Profile</h3>
            <div class="mb-3">
            <label for="" class="form-label">Nama Lengkap</label>
            <input type="text" class="form-control" name="name" id="" >
            </div>
            <div class="mb-3">
            <label for="" class="form-label">NIS/NIP</label>
            <input type="text" class="form-control" name="nis" id="" >
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Level</label>
              <select class="form-control" name="level" id="" style="height: 40px">
                  <option selected>--Pilih Level--</option>
                 <option value="pembimbing">Pembimbing</option>
                 <option value="siswa">Siswa</option>
              </select>
            </div>
        </div>
        </div>
        <div class="card">
        <div class="card-body">
         <h3>User Information</h3>
            <div class="mb-3">
              <label for="" class="form-label">Username</label>
              <input type="text" class="form-control" name="username" >
            </div>
            <div class="mb-3">
              <label for="" class="form-label">email</label>
              <input type="email" class="form-control" name="email" >
            </div>
            <div class="mb-3">
              <label for="" class="form-label">password</label>
              <input type="password" class="form-control" name="password" >
            </div>
        </div>
        </div>
          <button type="submit" class="btn btn-primary">Submit</button>
 
    </form>
      <a href="/siswaTables">Back</a>
</div>
@endsection