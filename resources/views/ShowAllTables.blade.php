@extends('layouts.pembimbing.dashboard')

@section('body')
<div class="container mt-2">
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
            <div class="card-body">
                <h3 class="card-title"><center>Tabel Siswa</center></h3>
                <table class="table table-striped|sm|bordered|hover|inverse table-inverse table-responsive">
                    <thead class="thead-inverse|thead-default">
                        <tr>
                            <th>NIS</th>
                            <th>Nama Lengkap</th>
                            <th>Kelas</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($siswa as $siswa)
                                <tr>
                                    <td>{{ $siswa->nis }}</td>
                                    <td>{{ $siswa->name }}</td>
                                    <td>{{ $siswa->username }}</td>
                                    <td>
                                        <a href="/detail/{{ $siswa->id }}" class="btn btn-primary btn-sm" >Detail</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
                
            </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
            <div class="card-body">
                <h3 class="card-title"><center>Tabel Pembimbing</center></h3>
                <table class="text-center table table-striped|sm|bordered|hover|inverse table-inverse table-responsive">
                    <thead class="thead-inverse|thead-default">
                        <tr>
                            <th>NIP</th>
                            <th>Nama Lengkap</th>
                            <th>Kejuruan</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($pembimbing as $pembimbing)
                                <tr>
                                    <td>{{ $pembimbing->nip }}</td>
                                    <td>{{ $pembimbing->name }}</td>
                                    <td>{{ $pembimbing->jurusan->jurusan }}</td>
                                    <td><a href="/detailPembimbing/{{ $pembimbing->id }}">Detail</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
            </div>
            <div class="card mt-5">
            <div class="card-body">
                <h3 class="card-title"><center>Tabel Perusahaan</center></h3>
                @if (Auth()->user()->level == 'admin')
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">+ Perusahaan</button>
                @endif
                <table class="table table-striped|sm|bordered|hover|inverse table-inverse table-responsive">
                    <thead class="thead-inverse|thead-default">
                        <tr>
                            <th>Nama Perusahaan</th>
                            <th>Alamat Perusahaan</th>
                            <th>jurusan</th>
                            <th>Kerjasama</th>
                            @if (Auth()->User()->level == 'admin') 
                                <th>Aksi</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($perusahaan as $perusahaan)
                                <tr>
                                    <td>{{ $perusahaan->nama_perusahaan }}</td>
                                    <td>{{ $perusahaan->alamat_perusahaan }}</td>
                                    <td>{{ $perusahaan->kejuruan }}</td>
                                    <td>{{ $perusahaan->kerjasama }}</td>
                                    <td>
                                        @if (Auth()->User()->level == 'admin') 
                                        <a href="/deletePerusahaan/{{ $perusahaan->id }}">Hapus</a></td>
                                        @endif
                                </tr>
                            @endforeach
                        </tbody>
                </table>

            </div>
            </div>
        </div>
        
        </div>
        <button onclick="kembali()" class="btn btn-danger">Kembali</button>
        <script>function kembali(){
            window.history.back();
        }</script>
        </div>
</div>



                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New Company</h5>
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/createPerusahaan" method="post">
                                @csrf
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Nama Perusahaan:</label>
                                <input type="text" class="form-control" id="recipient-name" name="nama_perusahaan">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Alamat Perusahaan:</label>
                                <input type="text" class="form-control" id="recipient-name" name="alamat_perusahaan">
                            </div>
                            <div class="form-group">
                                <label for="">Kerjasama dengan Keahlian : </label><br>
                                @foreach ($jurusan as $item)
                                    <input type="checkbox" name="jurusan[]" value="{{ $item->jurusan }}">{{ $item->jurusan }}<br>                             
                                @endforeach
                            </div>
                            <div class="form-group">
                                <label for="">Kerjasama Di bidang: </label><br>
                                    <input type="checkbox" name="kerjasama[]" value="Rekruitmen">Rekruitmen<br>                            
                                    <input type="checkbox" name="kerjasama[]" value="Prakerin">Prakerin<br>                             
                                    <input type="checkbox" name="kerjasama[]" value="Penyelarasan Kurikulum">Penyelarasan Kurikulum<br>                             
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                        </div>
                    </div>
                    </div>
@endsection