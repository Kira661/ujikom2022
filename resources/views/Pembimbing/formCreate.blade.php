@extends('layouts.app')

@section('content')
<div class="container">
    <form action="/createPembimbing" method="POST">
        @csrf
        <input type="hidden" name="level" value="pembimbing">
    <div class="mb-3">
      <label for="" class="form-label">Nama Pembimbing</label>
      <input type="text" class="form-control" name="name" id=""  placeholder="Nama Pembimbing">
    </div>
    <div class="mb-3">
      <label for="" class="form-label">NIP Pembimbing</label>
      <input type="number" class="form-control" name="nip" id=""  placeholder="NIP Pembimbing">
    </div>
    <div class="mb-3">
      <label for="" class="form-label">Kejuruan</label>
      <input type="text" class="form-control" name="kejuruan" id=""  placeholder="kejuruan">
    </div>
    <div class="mb-3">
      <label for="" class="form-label">Username</label>
      <input type="text" class="form-control" name="username" id=""  placeholder="username">
    </div>
    <div class="mb-3">
      <label for="" class="form-label">email</label>
      <input type="email" class="form-control" name="email" id=""  placeholder="email">
    </div>
    <div class="mb-3">
      <label for="" class="form-label">password</label>
      <input type="password" class="form-control" name="password" id=""  placeholder="password">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection